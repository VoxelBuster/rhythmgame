import random

import cocos as c2d
import pygame

import util

keys = ['a', 's', 'd', 'f', 'j', 'k', 'l', ';']
hit_flash_opacity = [255] * 8
requires_release = [False] * 8

notes_onscreen = []  # [track (0-7), y (px), length (sec)]

clock = pygame.time.Clock()

screen_w = 496
screen_h = 720

fps_limiter = 60

score = 0

difficulty = 8  # number of keys
velocity = 360.0 / float(fps_limiter)  # pixels per frame
hit_line_y = 100

# 'flawless' is frame-perfect
note_perfect_time = 2.0 / float(fps_limiter)  # time in seconds that the player has before or after a note to score 'perfect'
note_great_time = 4.0 / float(fps_limiter)  # 'great'
note_good_time = 8.0 / float(fps_limiter)  # 'good'
note_ok_time = 12.0 / float(fps_limiter)  # 'fast/slow'

flawless_value = 200
perfect_value = 100
great_value = 75
good_value = 50
ok_value = 10
fail_value = 0


class Game(c2d.layer.Layer):
    is_event_handler = True

    def __init__(self):
        super(Game, self).__init__()
        self.keys_pressed = set()
        self.feedback_layer = None
        self.feedback_opacity = 0

    def check_notes_hit(self):
        global score
        notes_by_track = [[]] * 8
        for note in [n for n in notes_onscreen]:
            notes_by_track[note[0]].append(note)
        for track in notes_by_track:
            if not track:
                continue
            idx = track[0][0]
            lowest_note_on_track = None
            for note in track:
                if lowest_note_on_track is None:
                    lowest_note_on_track = note
                else:
                    if note[1] < lowest_note_on_track[1]:
                        lowest_note_on_track = note
            if lowest_note_on_track is None:
                continue
            if keys[idx] in [chr(k) for k in self.keys_pressed] and not requires_release[idx]:
                dist_from_hitline = abs(lowest_note_on_track[1] - hit_line_y)
                requires_release[idx] = True
                if dist_from_hitline <= velocity:
                    score += flawless_value
                    notes_onscreen.remove(lowest_note_on_track)
                    self.feedback_layer = c2d.text.Label('Flawless!', (0, 0), color=(255, 127, 255, 255), font_size=18,
                                                         font_name='Arial')
                    self.feedback_opacity = 255
                elif dist_from_hitline <= note_perfect_time * fps_limiter * velocity:
                    score += perfect_value
                    notes_onscreen.remove(lowest_note_on_track)
                    self.feedback_layer = c2d.text.Label('Perfect!', (0, 0), color=(255, 255, 0, 255), font_size=18,
                                                         font_name='Arial')
                    self.feedback_opacity = 255
                elif dist_from_hitline <= note_great_time * fps_limiter * velocity:
                    score += great_value
                    notes_onscreen.remove(lowest_note_on_track)
                    self.feedback_layer = c2d.text.Label('Great!', (0, 0), color=(0, 255, 0, 255), font_size=18,
                                                         font_name='Arial')
                    self.feedback_opacity = 255
                elif dist_from_hitline <= note_good_time * fps_limiter * velocity:
                    score += good_value
                    notes_onscreen.remove(lowest_note_on_track)
                    self.feedback_layer = c2d.text.Label('Good', (0, 0), color=(0, 127, 127, 255), font_size=18,
                                                         font_name='Arial')
                    self.feedback_opacity = 255
                elif dist_from_hitline <= note_ok_time * fps_limiter * velocity:
                    score += ok_value
                    notes_onscreen.remove(lowest_note_on_track)
                    self.feedback_layer = c2d.text.Label('Okay', (0, 0), color=(196, 196, 196, 255), font_size=18,
                                                         font_name='Arial')
                    self.feedback_opacity = 255
                else:
                    score += fail_value
                    notes_onscreen.remove(lowest_note_on_track)
                    self.feedback_layer = c2d.text.Label('Fail', (0, 0), color=(255, 0, 0, 255), font_size=18,
                                                         font_name='Arial')
                    self.feedback_opacity = 255

    def draw(self):
        clock.tick(fps_limiter)
        self.children = list()
        hit_line = c2d.layer.ColorLayer(255, 0, 0, 255, width=screen_w, height=1)
        hit_line.position = (0, hit_line_y)
        for i in range(len(hit_flash_opacity)):
            hit_flash_opacity[i] -= 32
            if hit_flash_opacity[i] < 0:
                hit_flash_opacity[i] = 0
            hit_flash = c2d.layer.ColorLayer(255, 255, 255, hit_flash_opacity[i], width=int(screen_w / 8),
                                             height=screen_h)
            hit_flash.position = (i * (screen_w / 8), 0)
            self.add(hit_flash, z=1)
        for key in self.keys_pressed:
            keychar = chr(key)
            if keychar not in keys:
                continue
            hit_flash_opacity[keys.index(keychar)] = 255

        for note in [n for n in notes_onscreen]:
            min_note_height = int(velocity * note_perfect_time * float(fps_limiter))
            if note[2] == 0:
                note_height = min_note_height
            else:
                note_height = int(velocity * note[2] * fps_limiter)
            note_rgb = util.hsv2rgb(float(note[0] + 1) / 8.0, 1, 1)
            note_layer = c2d.layer.ColorLayer(note_rgb[0], note_rgb[1], note_rgb[2], 255, width=int(screen_w / 8),
                                              height=note_height)
            note_layer.position = (note[0] * (screen_w / 8), note[1])
            self.add(note_layer, z=16)
            note[1] -= velocity
            if note[1] < 0:
                notes_onscreen.remove(note)

        # === temporary code ===
        r = random.randint(1, fps_limiter * 2)
        if r == fps_limiter:
            notes_onscreen.append([random.randint(0, 7), screen_h, 0])
        # === end temp code ===

        if self.feedback_layer is not None:
            self.feedback_layer.opacity = self.feedback_opacity
            self.feedback_layer.position = ((screen_w / 2) - (self.feedback_layer.element.content_width / 2), screen_h - 100)
            self.add(self.feedback_layer, z=125)
            if self.feedback_opacity > 0:
                self.feedback_opacity -= 8
                if self.feedback_opacity < 0:
                    self.feedback_opacity = 0
        self.add(hit_line, z=126)

        score_counter = c2d.text.Label("Score: " + str(score), (25, screen_h - 25), color=(255, 0, 0, 255))
        self.add(score_counter, z=127)

        self.check_notes_hit()

    def on_key_press(self, key, modifiers):
        global score, notes_onscreen
        if 'r' in chr(key):
            score = 0
            notes_onscreen = []
        self.keys_pressed.add(key)

    def on_key_release(self, key, modifiers):
        if chr(key) in keys:
            requires_release[keys.index(chr(key))] = False
        self.keys_pressed.remove(key)


if __name__ == '__main__':
    c2d.director.director.init(width=screen_w, height=screen_h)
    game_layer = Game()
    main_scene = c2d.scene.Scene(game_layer)
    c2d.director.director.run(main_scene)
