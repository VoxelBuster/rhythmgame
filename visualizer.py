import audio_manager
import cocos as c2d
import pygame
import time
import math
import util

fps_limiter = 60
clock = pygame.time.Clock()

am = audio_manager.AudioManager()

song_path = 'test_song.mp3'

screen_w = 496
screen_h = 720

hit_line_y = 512

rolling_mean_frames = 3
bar_rolling_mean = [[0] * rolling_mean_frames] * 8
rolling_max = False

volume_mod = 0.5


def time_ms():
    return round(time.time() * 1000)


def translate(value, left_min, left_max, right_min, right_max):
    left_span = left_max - left_min
    right_span = right_max - right_min

    value_scaled = float(value - left_min) / float(left_span)

    return right_min + (value_scaled * right_span)


class Game(c2d.layer.Layer):
    is_event_handler = True

    def __init__(self):
        super(Game, self).__init__()
        self.keys_pressed = set()
        self.feedback_layer = None
        self.feedback_opacity = 0

        self.frame = 0
        self.start_time = 0

        self.hit_line = c2d.layer.ColorLayer(255, 0, 0, 255, width=screen_w, height=1)
        self.hit_line.position = (0, hit_line_y)

    def calc_audio(self, n_samples=512):
        start_idx = int((time_ms() - self.start_time) / 1000 * 44100)
        try:
            fft = am.fft_audio_block(start=start_idx, n=n_samples)
        except ValueError as e:
            print(e)
            print("Cannot read audio samples from index {0} to {1}".format(start_idx, start_idx + n_samples))
            return [0] * n_samples

        normalized = []
        for freq in fft:
            normalized.append(abs(freq))

        freq_vol = [0] * 8
        for x in range(8):
            start = x * 64
            if x == 0:
                freq_vol[x] = max(normalized[start + 16:start + 64]).real * 10 ** 13
            else:
                freq_vol[x] = max(normalized[start:start + 64]).real * 10 ** 13

        y = [0] * 8
        y[0] = int(translate(math.sqrt(freq_vol[0]) * volume_mod if freq_vol[0] > 0 else 0, 134, 308, 0, screen_h))
        y[1] = int(translate(math.sqrt(freq_vol[1]) * volume_mod if freq_vol[1] > 0 else 0, 48.9, 141.42, 0, screen_h))
        y[2] = int(translate(math.sqrt(freq_vol[2]) * volume_mod if freq_vol[2] > 0 else 0, 31.6, 66.33, 0, screen_h))
        y[3] = int(translate(math.sqrt(freq_vol[3]) * volume_mod if freq_vol[3] > 0 else 0, 4.4, 24.5, 0, screen_h))
        y[4] = int(translate(math.sqrt(freq_vol[4]) * volume_mod if freq_vol[4] > 0 else 0, 6.3, 24.9, 0, screen_h))
        y[5] = int(translate(math.sqrt(freq_vol[5]) * volume_mod if freq_vol[5] > 0 else 0, 28.2, 44.75, 0, screen_h))
        y[6] = int(translate(math.sqrt(freq_vol[6]) * volume_mod if freq_vol[6] > 0 else 0, 59.1, 94.9, 0, screen_h))
        y[7] = int(translate(math.sqrt(freq_vol[7]) * volume_mod if freq_vol[7] > 0 else 0, 223, 500, 0, screen_h))

        return y

    def draw(self):
        clock.tick(fps_limiter)

        self.children = list()

        if am.is_playing():
            y_vals = self.calc_audio()

        # print(y_vals)

        for i in range(len(y_vals)):
            for j in reversed(range(1, len(bar_rolling_mean[i]))):
                bar_rolling_mean[i][j] = bar_rolling_mean[i][j - 1]

            bar_rolling_mean[i][0] = y_vals[i]

            bar_mean = sum(bar_rolling_mean[i]) // rolling_mean_frames if not rolling_max else max(bar_rolling_mean[i])

            bar_rgb = util.hsv2rgb((i + 1) / 8.0, 1, 1)
            bar_layer = c2d.layer.ColorLayer(bar_rgb[0], bar_rgb[1], bar_rgb[2], 255, width=int(screen_w / 8),
                                             height=bar_mean)
            bar_layer.position = (i * (screen_w / 8), 0)
            self.add(bar_layer, z=16)

        self.add(self.hit_line, z=126)

        self.frame += 1


if __name__ == '__main__':
    # FFMPEG must be on path
    am.load(song_path, into_mixer=True)

    c2d.director.director.init(width=screen_w, height=screen_h)
    game_layer = Game()
    main_scene = c2d.scene.Scene(game_layer)

    am.play()
    game_layer.start_time = time_ms()
    c2d.director.director.run(main_scene)
